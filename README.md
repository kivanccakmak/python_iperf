#python_iperf
------------
This repository is created to observe splitting a tcp connection via emulation approach.  

Assume that we have a Content Server, Proxy Server and portable client.
Content Server connects to Proxy Server via ethernet interface which is more roboust and 
higher bandwidth link.  
On the other hand, proxy server connets to portable client via wlan which has fluctuating bandwidth.    
During experiments, we'll use a laptop for all three of them. Surely, we need to have a 
wireless AP to connect Proxy Server and portable client.    

In order to emulate breaking, I propose to do following:  

(1)Portable client waits a connection from Proxy Server using iperf    
(2)Proxy server waits a connection from Content Server using iperf    
(3)Content Server initiates data transmission to Proxy Server using iperf  
(4)Proxy server reads received data from iperf (by parsing .txt file in Python)  
(5)Proxy server opens a socket to forward data to portable client(by Python)
(6)Proxy server calculates buffer from total received data and forwarded data
(7)If buffer is higher than upper_threshold(that defined in bash file) 
proxy server adds a delay to ethernet link by usage of network emulator(netem)     
(7)If buffer is less than lower threshold, proxy server decreases(if total delay  
was greater than 0 at that time) again by usage of network emulator(netem)  
delay of ethernet link  

Before running bash files, first become a root and change permissions as following:

```
sudo chmod u+x delay.sh;
sudo chmod u+x parallel.sh;
sudo chmod u+x initialize.sh
```
Below,

I'll use PC 1 for server, PC 2 for proxy server and PC 3 for portable client

```
(PC 3) iperf -s -p5001
(PC 2) ./parallel.sh <window_type> <interference> <port_quantity> <forward_host_IP> <base_port>
(PC 1) iperf -c IP_ADDRESS_OF_PC2
(PC 2) ./delay.sh   #uses netem to provide adaptive delay to eth0  
```

**window type**: TCP window type that forwarding proxy uses  
**interference**: indicator denotes whether experiments includes interference(and records differently)  
**port quantity**: number of ports that proxy server would open forward_host_IP: IP address of PC 3     
**base port**: ports base port + 1 : base port + 1 + port quantity would be open to forward, base_port would be     listened for incoming iperf connection from server    

Followingly, outputs would be recorded in directories in a systematic manner.    

PS: delay.sh is not fully automized


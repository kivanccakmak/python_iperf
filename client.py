#!/usr/bin/env/python
import errno
import socket
import sys
import os
import datetime
import math
import time
import iperf_read
import timeit
import read_transmissions
dict = {'forward_host': 1, 'forward_port': 2, 'port_quantity':3, 'r_fly':4, 'r_fwd':5, 
    'r_detail':6, 'recv':7, 'dir_name':8};
max_size = 1 * math.pow(10,6); # 10 MByte
def client(data_size, client_sock, forward_port, tic, record_folder, dir_name):
    print("Creating the data buffer ...")
    buf = memoryview(bytearray(int(data_size))) # Create the data buffer.
    try:
    # Start sending data.
        tot_send_bytes = 0
        while data_size > tot_send_bytes:
            send_bytes = client_sock.send(buf[tot_send_bytes:]) 
            tot_send_bytes += send_bytes
            print("Send {} MBytes".format(send_bytes / math.pow(10,6) ))
            tx_time = (timeit.default_timer() - tic);
            os.chdir(dir_name)
            file_append = open(record_folder, "a");
            file_append.write("time = {} transmitted = {} port = {} \n".format(tx_time, str(send_bytes), forward_port));
            file_append.close()
            os.chdir('../')
    except socket.error as se:
        if se.errno == errno.ECONNRESET: print("Connection is reset.")
        else: print("Connection failure: {}".format(se))

def main():
    if len(sys.argv) != 10:
        print("Usage: {} <forward_host> <forward_port> <port_quantity> <fly_bits_folder> "\
    "<fwd_bits_folder> <fwd_detail_folder> <received_folder> <dir_name> <duration> ".format(sys.argv[0]))
        sys.exit(1)
    print(sys.argv[1:])
    #INITIALIZATION
    buffer = 0
    time_flag = True #checks whether duration elapsed or not
    forwarded_total = 0#records how much cumulative data transferred from this socket
    forwarded_all = 0#records how much data transferred from all sockets
    tic = timeit.default_timer() #to count run duration 
    forward_host = sys.argv[1];#local ip of forward destination
    forward_port = sys.argv[2];#port of destinaton
    port_quantity = sys.argv[3];#number of ports (this socket would get 1/port_quantity of buffer at each round
    fly_folder = sys.argv[4];#.txt file that updates byt in flight
    fwd_folder = sys.argv[5];#txt file that updates total forwarded bytes
    record_folder = sys.argv[6];
    recv_folder = sys.argv[7];#txt file that records total received bytes from iperf application
    dir_name = sys.argv[8];#name of directory that all .txt outputs of this function recorded
    duration = int(sys.argv[9]);
    print("Socket is creating")
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
    client_sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    print (forward_host)
    print (int(forward_port))
    client_sock.connect((forward_host, int(forward_port)))
    print("Socket is created")
    #==========================================================================
    while (time_flag):
        print("duration = {}".format(duration))
        passed = float(timeit.default_timer() - tic);
        print("passed = {}".format(passed));
        print(passed < duration)
        time_flag = ( int(math.ceil(passed)) < duration)
        forwarded_all = read_transmissions.read_transmissions(dir_name);
        received_total = iperf_read.iperf_read(recv_folder)
        #print("received_total = {}".format(received_total))
        #print("forwarded_total = {}".format(forwarded_total))
        #print("time_elapsed = {}".format(timeit.default_timer() - tic))
        buffer = received_total - forwarded_all; 
        print("buffer = {}".format(buffer))
        if  buffer > 0:
            data_size = min(buffer/int(port_quantity), max_size); #bytes
            os.chdir(dir_name)
            fly = open(fly_folder, "wb")
            fly.write("{}\n".format(data_size))
            fly.close()
            os.chdir('../')
            client(data_size, client_sock, int(forward_port), tic, record_folder, dir_name)
            forwarded_total += data_size;
            os.chdir(dir_name)
            output = open(fwd_folder, "wb")
            output.write("{}\n".format(forwarded_total))
            output.close()
            os.chdir('../')
            print("buffer = {}".format(buffer))        
    client_sock.close()
    print("socket is closed")
    #==========================================================================
    print("time elapsed: {}".format(timeit.default_timer()-tic))
    print("data transmitted: {} MByte".format(forwarded_total / math.pow(10,6)))
    print("rate: {} Mbps".format(8 * forwarded_total / (math.pow(10,6) * (timeit.default_timer()-tic))))
    print("Closing the socket ...");
    
if __name__ == "__main__":
    main()
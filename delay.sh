#eth0 RTT changing
passed=0
delay=10
duration=110
time=0
dir_name="int_1_port"
report_file="wired_received.txt"
low_threshold=10000000 #10 MB
up_threshold=50000000  #50 MB
check_interval=5 #second
delay_interval=5 #ms
delay_current=0 #ms
delay_minimum=0
time=$(($time+$delay))
echo "========================kivanc=========================="
while true;
do	
	echo "hello kivanc"
	received=$(python iperf_read.py $report_file)
	forwarded=$(python read_transmissions.py $dir_name)
	buffer=$(($received-$forwarded))
	if [ "$buffer" -lt "$low_threshold" ];
	then
		echo "less than low threshold"
		if [ "$delay_current" -ne "$delay_minimum" ];
		then
			delay_current=$(($delay_current-$delay_interval))
		fi
	elif [ "$buffer" -gt "$up_threshold" ];
	then
		echo "more than up threshold"
		delay_current=$(($delay_current+$delay_interval))
	else
		echo "in range"
		if [ "$delay_current" -ne "$delay_minimum" ];
		then
			delay_current=$(($delay_current-$delay_interval))
		fi
	fi	
	tc qdisc add dev eth0 root netem delay "$delay_current""ms"
	sleep $(($check_interval))	
	tc qdisc del dev eth0 root netem #to delete
	echo "                        delay = ""$delay_current""            "
	echo "                        buffer = ""$buffer""                  "
	echo "                        low_th = ""$low_threshold""           "
	echo "                        up_th = ""$up_threshold""           "
	time=$(($time+$check_interval))  
	if [ "$time" -gt "$duration" ];
	then
		break;
	fi
done

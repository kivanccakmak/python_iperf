#!/bin/bash
#1-this bash enables usage of dummy packet construction application [iperf] and 
#loadable linux kernel module [tcp_probe] which provides instantenous congestion window sizes and python
#to transmit data
if [ "$#" -ne 5 ];
	then
	echo "illegal usage"
	echo "./parallel.sh <window_type> <interference> <port_quantity> <forward_host> <base_port>"
	echo "example with interference: ./parallel.sh cubic int 4 192.168.2.1 5001 "
	echo "example without interference: ./parallel.sh cubic non 4 192.168.2.1 5001"
	exit 1
fi

#get positional variables
window_type=$1
interference=$2
port_quantity=$3
forward_host=$4
base_port=$5

if [ "$interference" == "int" ];
then
	dir_name="int_""$port_quantity""_port"
	echo "interference network exists"
elif [ "$interference" == "non" ]; 
then
	dir_name="non_""$port_quantity""_port"
	echo "interference network does not exist"
else
	echo "second positional variable has to be 'int' or 'non'"
fi

echo "window type is changing to $window_type";
echo $window_type > /proc/sys/net/ipv4/tcp_congestion_control;
echo "record directory = ""$dir_name"
mkdir ${dir_name};
#INITILIZATION 1 -> RECORD OUTPUT FILES
for((counter=1;counter<=$port_quantity;counter++))
do
	port=$(($base_port+$counter))
	forward="fwd_""$port"".txt";
	fly="fly_""$port"".txt"
	record="port_""$port""_detail.txt"
	echo "1" > "$dir_name""/""$forward"
	echo "1" > "$dir_name""/""$fly"
	echo "" > "$dir_name""/""$record"
done



	




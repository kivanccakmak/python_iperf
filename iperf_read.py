import math
import sys
import os
dict = {'MBytes': math.pow(10, 6), 'KBytes': math.pow(10, 3)}
def iperf_read(iperf_output):
    #This function returns number of Bytes from iperf output
    #STEP 1 -> do not read lines [0, 5], since those are headers
    #STEP 2 -> start reading
    #STEP 2a -> split the line
    #STEP 2b -> check whether it's in first 10 seconds or not by (counter > 14)
    #since character size increases one and start-stop gap becomes non-empty when time
    #passes from 9 to 10
    #STEP 2c -> check whether throughput is Mbps or Kbps
    #STEP 2d -> add lines throughput(particular time interval's) to sum throughput
    iperf = open(iperf_output, 'r')
    summation = 0
    counter = 0
    for line in iperf:
        content = line.split()#STEP2a
        if counter > 5:
            last = content[len(content)-1];
            condition = last[len(last)-1] == 'c';
            if condition: #if (line contains characters)
                if counter > 14:#STEP2b
                    if content[2][0:3] == '0.0':
                        iperf.close()
                        return int(str(summation)[0:str(summation).find('.')])
                    summation += float(content[4]) * dict[content[5]]#2c and #2d
                else:
                    summation += float(content[5]) * dict[content[6]]#2c and #2d
        counter += 1
    iperf.close()
    return int(str(summation)[0:str(summation).find('.')])

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <file_name.txt> ".format(sys.argv[0]))
        sys.exit(1)
    thr = iperf_read(sys.argv[1])
    print "{}".format(thr)
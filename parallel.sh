#!/bin/bash
#1-this bash enables usage of dummy packet construction application [iperf] and 
#loadable linux kernel module [tcp_probe] which provides instantenous congestion window sizes and python
#to transmit data
delay=10 #seconds, duration of delay in between wired iperf and wireless python 
duration=110 #seconds, duration of forwarding run
if [ "$#" -ne 5 ];
	then
	echo "illegal usage"
	echo "./parallel.sh <window_type> <interference> <port_quantity> <forward_host> <base_port>"
	echo "example with interference: ./parallel.sh cubic int 4 192.168.2.1 5001 "
	echo "example without interference: ./parallel.sh cubic non 4 192.168.2.1 5001"
	exit 1
fi
./initialize.sh $1 $2 $3 $4 $5
window_type=$1
interference=$2
port_quantity=$3
forward_host=$4
base_port=$5
time=0
if [ "$interference" == "int" ];
then
	dir_name="int_""$port_quantity""_port"
else
	dir_name="non_""$port_quantity""_port"
fi	
echo "iperf receiving is starting"
server="192.168.2.44"
report_file="wired_received.txt"
iperf -s -p $base_port -i1 -t110 &> $report_file & 
iperf_pid=$!
sleep $(($delay))
for((counter=1;counter<=$port_quantity;counter++))
do
	PORT_NUMBER=$(($base_port+$counter))
	fwd="fwd_""$PORT_NUMBER"".txt"; #.txt file that records transmitted Bytes 
	fly="fly_""$PORT_NUMBER"".txt" #.txt file that record bits in flight in terms of Byte
	record="port_""$PORT_NUMBER""_detail.txt" #.txt file records each python transmission
	python client.py $forward_host $PORT_NUMBER $port_quantity $fly $fwd $record $report_file $dir_name $duration
done


from __future__ import print_function
import glob
import os
import iperf_read
import python_read
import sys

def read_transmissions(dir_name):
    transmitted_total = 0
    content = all(dir_name)
    if content:
        os.chdir(dir_name)
        file = glob.glob("*.txt")
        os.chdir('../')
        name = ''
        result = ''
        for counter in range(0,len(file)):
            name = file[counter];
            if name[0:3] == 'fwd' or name[0:3] == 'fly':
                transmitted_total += float(python_read.python_read(dir_name, name)) 
    else:
        print("file <{}> is empty".format(name))
        sys.exit(1)
    value = str(transmitted_total)
    if value.find('.') !=-1:
    	value = value[0:value.find('.')]
        transmitted_total = int(value)
    
    return transmitted_total;
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <directory_name> ".format(sys.argv[0]))
        sys.exit(1)
    transmitted_total = read_transmissions(sys.argv[1])
    print("{}".format(transmitted_total))
    
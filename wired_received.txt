------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 192.168.2.34 port 5001 connected with 192.168.2.44 port 30861
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0- 1.0 sec  4.42 MBytes  37.1 Mbits/sec
[  4]  1.0- 2.0 sec  10.1 MBytes  84.6 Mbits/sec
[  4]  2.0- 3.0 sec  9.87 MBytes  82.8 Mbits/sec
[  4]  3.0- 4.0 sec  9.78 MBytes  82.1 Mbits/sec
[  4]  4.0- 5.0 sec  10.1 MBytes  84.4 Mbits/sec
[  4]  5.0- 6.0 sec  9.80 MBytes  82.2 Mbits/sec
[  4]  6.0- 7.0 sec  9.93 MBytes  83.3 Mbits/sec
[  4]  7.0- 8.0 sec  9.83 MBytes  82.5 Mbits/sec
[  4]  8.0- 9.0 sec  9.94 MBytes  83.4 Mbits/sec
[  4]  9.0-10.0 sec  9.80 MBytes  82.2 Mbits/sec
[  4] 10.0-11.0 sec  9.96 MBytes  83.6 Mbits/sec
[  4] 11.0-12.0 sec  8.62 MBytes  72.3 Mbits/sec
[  4] 12.0-13.0 sec  7.17 MBytes  60.2 Mbits/sec
[  4] 13.0-14.0 sec  6.99 MBytes  58.7 Mbits/sec
[  4] 14.0-15.0 sec  7.26 MBytes  60.9 Mbits/sec
[  4] 15.0-16.0 sec  7.25 MBytes  60.8 Mbits/sec
[  4] 16.0-17.0 sec  6.73 MBytes  56.5 Mbits/sec
[  4] 17.0-18.0 sec  5.50 MBytes  46.2 Mbits/sec
[  4] 18.0-19.0 sec  5.51 MBytes  46.2 Mbits/sec
[  4] 19.0-20.0 sec  5.51 MBytes  46.2 Mbits/sec
[  4] 20.0-21.0 sec  5.48 MBytes  46.0 Mbits/sec
[  4] 21.0-22.0 sec  5.19 MBytes  43.5 Mbits/sec
[  4] 22.0-23.0 sec  3.84 MBytes  32.2 Mbits/sec
[  4] 23.0-24.0 sec  3.93 MBytes  33.0 Mbits/sec
[  4] 24.0-25.0 sec  3.92 MBytes  32.9 Mbits/sec
[  4] 25.0-26.0 sec  4.00 MBytes  33.6 Mbits/sec
[  4] 26.0-27.0 sec  2.52 MBytes  21.1 Mbits/sec
[  4] 27.0-28.0 sec  2.95 MBytes  24.8 Mbits/sec
[  4] 28.0-29.0 sec  3.02 MBytes  25.3 Mbits/sec
[  4] 29.0-30.0 sec  2.98 MBytes  25.0 Mbits/sec
[  4] 30.0-31.0 sec  3.00 MBytes  25.2 Mbits/sec
[  4] 31.0-32.0 sec  2.03 MBytes  17.1 Mbits/sec
[  4] 32.0-33.0 sec  2.27 MBytes  19.0 Mbits/sec
[  4] 33.0-34.0 sec  2.46 MBytes  20.6 Mbits/sec
[  4] 34.0-35.0 sec  2.40 MBytes  20.1 Mbits/sec
[  4] 35.0-36.0 sec  2.44 MBytes  20.4 Mbits/sec
[  4] 36.0-37.0 sec  1.75 MBytes  14.7 Mbits/sec
[  4] 37.0-38.0 sec  1.80 MBytes  15.1 Mbits/sec
[  4] 38.0-39.0 sec  1.95 MBytes  16.4 Mbits/sec
[  4] 39.0-40.0 sec  2.06 MBytes  17.3 Mbits/sec
[  4] 40.0-41.0 sec  2.00 MBytes  16.8 Mbits/sec
[  4] 41.0-42.0 sec  2.40 MBytes  20.1 Mbits/sec
[  4] 42.0-43.0 sec  1.73 MBytes  14.5 Mbits/sec
[  4] 43.0-44.0 sec  1.75 MBytes  14.7 Mbits/sec
[  4] 44.0-45.0 sec  1.75 MBytes  14.7 Mbits/sec
[  4] 45.0-46.0 sec  1.75 MBytes  14.7 Mbits/sec
[  4] 46.0-47.0 sec  1.38 MBytes  11.5 Mbits/sec
[  4] 47.0-48.0 sec  1.19 MBytes  9.96 Mbits/sec
[  4] 48.0-49.0 sec  1.50 MBytes  12.6 Mbits/sec
[  4] 49.0-50.0 sec  1.56 MBytes  13.1 Mbits/sec
[  4] 50.0-51.0 sec  1.50 MBytes  12.6 Mbits/sec
[  4] 51.0-52.0 sec  1.31 MBytes  11.0 Mbits/sec
[  4] 52.0-53.0 sec   960 KBytes  7.86 Mbits/sec
[  4] 53.0-54.0 sec  1.36 MBytes  11.4 Mbits/sec
[  4] 54.0-55.0 sec  1.28 MBytes  10.7 Mbits/sec
[  4] 55.0-56.0 sec  1.42 MBytes  11.9 Mbits/sec
[  4] 56.0-57.0 sec  1.38 MBytes  11.5 Mbits/sec
[  4] 57.0-58.0 sec  1.33 MBytes  11.2 Mbits/sec
[  4]  0.0-58.3 sec   252 MBytes  36.2 Mbits/sec
